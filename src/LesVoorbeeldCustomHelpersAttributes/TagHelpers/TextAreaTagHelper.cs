﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.Runtime.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using LesVoorbeeldCustomHelpersAttributes.Models;

using MyHowest;

namespace LesVoorbeeldCustomHelpersAttributes.TagHelpers
{
    //Example of how to extend an existing tagHelper (textarea)
    [HtmlTargetElement("textarea")]
    public class MyCustomTextArea : TextAreaTagHelper
    {
        public MyCustomTextArea(IHtmlGenerator generator) : base(generator)
        {
        }

        [HtmlAttributeName("asp-is-disabled")]
        public bool IsDisabled { set; get; }

        [HtmlAttributeName("asp-size-width")]
        public int Cols { get; set; }

        [HtmlAttributeName("asp-size-height")]
        public int Rows { get; set; }

        public Graad afstudeergraad { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.Attributes.Add(new TagHelperAttribute("cols", Cols));
            output.Attributes.Add(new TagHelperAttribute("rows", Rows));

            if (IsDisabled)
            {
                output.Attributes.Add(new TagHelperAttribute("disabled", "disabled"));
            }
            base.Process(context, output);
        }
    }


}
